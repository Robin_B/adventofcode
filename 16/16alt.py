from itertools import permutations 

lines = open("input.txt", "r").readlines()
valuableNodes = []
scan = {}
shortestPaths = []
start = ""
for line in lines:
    line = line.strip()
    name = line[6:8]
    if start == "": start = name
    rate = int(line[23:].partition(';')[0])
    if rate > 0:
        valuableNodes.append(name)
    valvesPos = line.find("valves")
    if valvesPos == -1:
        neighbours = [line[-2:]]
    else:
        neighbours = line[valvesPos + 7:].split(", ")
    #print(neighbours)
    scan[name] = (rate, neighbours)

print(scan, valuableNodes)

# find shortest paths to neighbors
paths = {}
for s in scan:
    for n in scan[s][1]:
        print(scan[s][1])
        if s > n:
            paths[n + s] = 1
        else:
            paths[s + n] = 1

improved = True
while improved:
    lenStart = len(paths)
    for p in list(paths):
        first = p[:2]
        second = scan[p[2:4]]
        for n in second[1]:
            if n == first: continue
            if first > n:
                if not n+first in paths or paths[n + first] > paths[p] + 1:
                    paths[n + first] = paths[p] + 1
            else:
                if not first + n in paths or paths[first + n] > paths[p] + 1:
                    paths[first + n] = paths[p] + 1
        
        first = p[2:4]
        second = scan[p[:2]]
        for n in second[1]:
            if n == first: continue
            if first > n:
                if not n+first in paths or paths[n + first] > paths[p] + 1:
                    paths[n + first] = paths[p] + 1
            else:
                if not first + n in paths or paths[first + n] > paths[p] + 1:
                    paths[first + n] = paths[p] + 1
    improved = len(paths) > lenStart
    print("calculating path ...", len(paths))

print("distances: ", paths)
perm = permutations(valuableNodes)

bestPressure = 0
bestOrder = ""

for p in perm:
    print("checking order",p)
    pos = start
    time = 30
    pressure = 0
    flowrate = 0
    for pp in p:
        if pp > pos:
            dist = paths[pos + pp]
        else:
            dist = paths[pp + pos]

        if dist >= time - 1:
            # no more time
            break
        time -= dist + 1
        pressure += flowrate * (dist + 1)
        flowrate += scan[pp][0]
        pos = pp

    #use up remaining time
    pressure += flowrate * time
    if pressure > bestPressure: 
        bestPressure = pressure
        bestOrder = p

print("Best pressure:", bestPressure, "order:", bestOrder)
