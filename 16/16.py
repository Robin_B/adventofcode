lines = open("input.txt", "r").readlines()
valuableNodes = []
scan = {}
shortestPaths = []
for line in lines:
    line = line.strip()
    name = line[6:8]
    rate = int(line[23:].partition(';')[0])
    if rate > 0:
        valuableNodes.append(name)
    valvesPos = line.find("valves")
    if valvesPos == -1:
        neighbours = [line[-2:]]
    else:
        neighbours = line[valvesPos + 7:].split(", ")
    #print(neighbours)
    scan[name] = (rate, neighbours)

print(scan, valuableNodes)

# find shortest paths to neighbors
paths = {}
for s in scan:
    for n in scan[s][1]:
        print(scan[s][1])
        if s > n:
            paths[n + s] = 1
        else:
            paths[s + n] = 1

improved = True
while improved:
    lenStart = len(paths)
    for p in list(paths):
        first = p[:2]
        second = scan[p[2:4]]
        for n in second[1]:
            if n == first: continue
            if first > n:
                if not n+first in paths or paths[n + first] > paths[p] + 1:
                    paths[n + first] = paths[p] + 1
            else:
                if not first + n in paths or paths[first + n] > paths[p] + 1:
                    paths[first + n] = paths[p] + 1
        
        first = p[2:4]
        second = scan[p[:2]]
        for n in second[1]:
            if n == first: continue
            if first > n:
                if not n+first in paths or paths[n + first] > paths[p] + 1:
                    paths[n + first] = paths[p] + 1
            else:
                if not first + n in paths or paths[first + n] > paths[p] + 1:
                    paths[first + n] = paths[p] + 1




    improved = len(paths) > lenStart
print("Paths:", paths)
time = 30
pos = "AA"
currentFlow = 0
pressure = 0

while time > 0:
    #consider all paths from pos
    highestVal = 0
    highestValPos = ""
    highestValDist = 0

    for s in scan:
        if s == pos: continue
        if s > pos:
            dist = paths[pos + s]
        else:
            dist = paths[s + pos]
        if dist >= time - 2: continue
        value = scan[s][0] * (time - 2 - dist) 
        if value > highestVal:
            highestVal = value
            highestValPos = s
            highestValDist = dist
    print("best value right now: going from", pos," to ", highestValPos, highestVal, "dist:", highestValDist)
    if highestVal == 0:
        highestValDist = time - 1
        highestValPos = "AA"
    time -= highestValDist + 1
    pressure += (highestValDist + 1) * currentFlow
    currentFlow += scan[highestValPos][0]
    print("min", 30-time, "pressure:", pressure, "flow:", currentFlow)
    scan[highestValPos] = (0, scan[highestValPos][1])
    pos = highestValPos


print("end! time: ", time, currentFlow, pressure)

