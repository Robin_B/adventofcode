lines = open("input.txt", "r").readlines()

walls = []
sx = 500
sy = 0
lowestWall = 0
floor = 0
sand = []
openSand = [(500, 0)]
def checkPos(x, y):
    global walls, sand, floor
    if y >= floor:
        return True
    for w in walls:
        if x < w[0] and x < w[2]: continue
        if x > w[0] and x > w[2]: continue
        if y < w[1] and y < w[3]: continue
        if y > w[1] and y > w[3]: continue
        return True
    if (x, y) in sand:
        return True
    return False

for l in lines:
    w = l.strip().split(" -> ")
    print(w)
    mprevx = 0
    mprevy = 0
    for we in w:
        m = [int(x) for x in we.split(",")]
        #print(m)
        if mprevx != 0:
            walls.append((mprevx, mprevy, m[0], m[1]))
        mprevx = m[0]
        mprevy = m[1]
        if m[1] > lowestWall: lowestWall = m[1]

floor = lowestWall + 2
print("Floor height: ", floor)

while len(openSand) > 0:
    #s = openSand.pop()
    s = openSand[0]
    openSand = openSand[1:]
    sand.append(s)
    print("checking",s, "sand:", len(sand))
    
    v = (s[0] - 1, s[1] + 1)
    if v not in openSand and not checkPos(s[0] - 1, s[1] + 1):
        openSand.append(v)
    
    v = (s[0] + 1, s[1] + 1)
    if v not in openSand and not checkPos(s[0] + 1, s[1] + 1):
        openSand.append(v)
    
    v = (s[0], s[1] + 1)
    if v not in openSand and not checkPos(s[0], s[1] + 1):
        openSand.append(v)


print("finished", len(sand))