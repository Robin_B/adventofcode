lines = open("input.txt", "r").readlines()

walls = []
sx = 500
sy = 0
lowestWall = 0
floor = 0
sand = []

def checkPos(x, y):
    global walls, sand
    for w in walls:
        if x < w[0] and x < w[2]: continue
        if x > w[0] and x > w[2]: continue
        if y < w[1] and y < w[3]: continue
        if y > w[1] and y > w[3]: continue
        return True
    if (x, y) in sand:
        return True
    return False

for l in lines:
    w = l.strip().split(" -> ")
    print(w)
    mprevx = 0
    mprevy = 0
    for we in w:
        m = [int(x) for x in we.split(",")]
        print(m)
        if mprevx != 0:
            walls.append((mprevx, mprevy, m[0], m[1]))
        mprevx = m[0]
        mprevy = m[1]
        if m[1] > lowestWall: lowestWall = m[1]
#print(walls, lowestWall)
floor = lowestWall + 2
#fall the sand

while sy < lowestWall:
    if checkPos(sx, sy + 1):
        if checkPos(sx - 1, sy + 1):
            if checkPos(sx + 1, sy + 1):
                # sand rests
                sand.append((sx, sy))
                sx = 500
                sy = 0
            else:
                sx += 1
                sy += 1
        else:
            sx -= 1
            sy += 1
    else:
        sy += 1

    print(len(sand))
print("finished")