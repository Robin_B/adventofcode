f = open("input.txt", "r")
score = 0
for line in f:
    line = line.strip()
    half = int(len(line)/2)
    a = line[:half]
    b = line[half:]
    for letter in a:
        #print("checking letter ", letter)
        if letter in b:
            value = ord(letter) - ord('A')+27
            if value > 52: value -= 58
            score += value
            print("found ", letter, value)
            break
    print(score)



