f = open("input.txt", "r")
score = 0
entries = []
subentry = []
for line in f:
    if len(subentry) == 3:
        entries.append(subentry)
        subentry = []
    subentry.append(line.strip())
entries.append(subentry)

for e in entries:
    for letter in e[0]:
        if letter in e[1] and letter in e[2]:
            value = ord(letter) - ord('A')+27
            if value > 52: value -= 58
            score += value
            break

print(score)