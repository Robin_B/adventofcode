f = open("input.txt", "r")
trees = []
for line in f:
    trees.append([int(s) for s in line.strip()])

maxVis = 0
for x in range(1, len(trees[0])-1):
    for y in range(1, len(trees)-1):
        print("looking at", x, y)
        a = 0
        for x2 in range(x + 1, len(trees[0])):
            a += 1
            if trees[x2][y] >= trees[x][y]:
                break

        b = 0
        for x2 in range(x - 1, -1, -1):
            b += 1
            if trees[x2][y] >= trees[x][y]:
                break
        
        c = 0
        for y2 in range(y + 1, len(trees)):
            c += 1
            if trees[x][y2] >= trees[x][y]:
                break

        d = 0
        for y2 in range(y - 1, -1, -1):
            d += 1
            if trees[x][y2] >= trees[x][y]:
                break        

        val = a * b * c * d
        if val > maxVis: maxVis = val
print(maxVis)