f = open("input.txt", "r")
trees = []
for line in f:
    trees.append([int(s) for s in line.strip()])

visible = len(trees) * 2 + len(trees[0]) * 2 - 4
for x in range(1, len(trees[0])-1):
    for y in range(1, len(trees)-1):
        vis = False
        print("looking at", x, y)
        for x2 in range(0, x + 1):
            if x2 == x: 
                vis = True
                break
            if trees[x2][y] >= trees[x][y]:
                break

        for x2 in range(len(trees[0])-1, x - 1, -1):
            if x2 == x: 
                vis = True
                break
            if trees[x2][y] >= trees[x][y]:
                break
        
        for y2 in range(0, y + 1):
            if y2 == y: 
                vis = True
                break
            if trees[x][y2] >= trees[x][y]:
                break

        for y2 in range(len(trees)-1, y - 1, -1):
            if y2 == y: 
                vis = True
                break
            if trees[x][y2] >= trees[x][y]:
                break
        
        if vis: visible+=1

print(visible)