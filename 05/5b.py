f = open("input.txt", "r")
stacks = [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[]]
for line in f:
    if '[' in line:
        for i in range(1, len(line), 4):
            if line[i] != ' ':
                stacks[int((i - 1)/4)].insert(0, line[i])

    if line[0] == 'm':
        s = line.split(' ')
        amount = int(s[1])
        origin = int(s[3]) - 1
        target = int(s[5]) - 1
        l = len(stacks[target])
        for i in range(0, amount):
            stacks[target].insert(l,stacks[origin].pop())

s = ''
for e in stacks:
    if len(e) > 0:
        s += e[-1]
print(s)