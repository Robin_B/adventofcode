import math

f = open("input.txt", "r")
hx = 0
hy = 0
tx = 0
ty = 0
dx = 0
dy = 0
positions = {}
positions[(0,0)] = 1
for line in f:
    words = line.split(' ')
    d = line[0]
    steps = int(words[1])
    dx = 0
    dy = 0
    if d == 'R': dx = 1
    if d == 'L': dx = -1
    if d == 'U': dy = -1
    if d == 'D': dy = 1
    for s in range(steps):
        hx += dx
        hy += dy
        distx = hx - tx
        disty = hy - ty
        if abs(distx) <= 1 and abs(disty) <= 1:
            continue
        if disty != 0:
            ty += int(math.copysign(1, disty))
        if distx != 0:
            tx += int(math.copysign(1, distx))
        positions[(tx, ty)] = 1
        print(d, s, steps, (hx, hy), (tx, ty))
print(len(positions))
print(positions)

