import math

f = open("input.txt", "r")
dx = 0
dy = 0
positions = {}
positions[(0,0)] = 1
tailx = [0,0,0,0,0,0,0,0,0,0]
taily = [0,0,0,0,0,0,0,0,0,0]
for line in f:
    words = line.split(' ')
    d = line[0]
    steps = int(words[1])
    dx = 0
    dy = 0
    if d == 'R': dx = 1
    if d == 'L': dx = -1
    if d == 'U': dy = -1
    if d == 'D': dy = 1
    for s in range(steps):
        tailx[0] += dx
        taily[0] += dy
        for t in range(1, len(tailx)):
            distx = tailx[t-1] - tailx[t]
            disty = taily[t-1] - taily[t]
            if abs(distx) <= 1 and abs(disty) <= 1:
                continue
            if disty != 0:
                taily[t] += int(math.copysign(1, disty))
            if distx != 0:
                tailx[t] += int(math.copysign(1, distx))
        positions[(tailx[9], taily[9])] = 1
print(len(positions))

