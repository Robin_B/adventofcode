f = open("input.txt", "r")
x = 1
cycle = 0

def crt():
    global cycle, x
    if abs((cycle % 40) - x - 1) <= 1: 
        print("#", end ='')
    else:
        print(".", end = '')
    if cycle % 40 == 0:
        print()

for line in f:
    words = line.strip().split(" ")
    cycle += 1
    crt()
    if words[0] == "addx":
        cycle += 1
        crt()
        x += int(words[1])
