import math

f = open("input.txt", "r")

x = 1
cycle = 0
score = 0
for line in f:
    words = line.strip().split(" ")
    cycle += 1
    if cycle % 20 == 0 and cycle % 40 != 0: score += cycle * x
    if words[0] == "addx":
        cycle += 1
        if cycle % 20 == 0 and cycle % 40 != 0: score += cycle * x
        x += int(words[1])
        print(x, score)
print(score)