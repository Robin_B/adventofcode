from itertools import permutations 

lines = open("input.txt", "r").readlines()
sides = [(-1,0,0),(1,0,0),(0,-1,0),(0,1,0),(0,0,-1),(0,0,1)]
cubes = []
visibleSides = 0
for line in lines:
    newcube = [int(x) for x in line.strip().split(",")]
    #print(newcube)
    for s in sides:
        if [newcube[0] + s[0], newcube[1] + s[1], newcube[2] + s[2]] not in cubes:
            visibleSides += 1
        else:
            visibleSides -= 1
    cubes.append(newcube)
print(visibleSides)