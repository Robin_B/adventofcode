from itertools import permutations 

lines = open("input.txt", "r").readlines()
sides = [(-1,0,0),(1,0,0),(0,-1,0),(0,1,0),(0,0,-1),(0,0,1)]
cubes = []
outsides = []
openOutsides = [[0,0,0]]
visibleSides = 0
for line in lines:
    cubes.append([int(x) for x in line.strip().split(",")])

while len(openOutsides) > 0:
    for openCube in list(openOutsides):
        openOutsides.remove(openCube)

        if openCube not in outsides: 
            outsides.append(openCube)
        else:
            continue
        for s in sides:
            c = [openCube[0] + s[0], openCube[1] + s[1], openCube[2] + s[2]]
            if c not in outsides and c not in openOutsides and c not in cubes and c[0] >= -1 and c[0] <= 20 and c[1] >= -1 and c[1] <= 20 and  c[2] >= -1 and c[2] <= 20:
                openOutsides.append(c)
            else:
                if c in cubes:
                    visibleSides += 1
print("calculated outside:", len(outsides))
print("visible:", visibleSides)