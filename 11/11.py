lines = open("input.txt", "r").readlines()

mitems = []
mop = []
mopval = []
test = []
trumonkey = []
falmonkey = []
inspectcount = []
prod = 1
monkeys = int((len(lines) + 1) / 7)
for m in range(monkeys):
    v = lines[m * 7 + 1].strip().replace(",","").split(" ")[2:]
    mitems.append([int(x) for x in v])
    mop.append(lines[m * 7 + 2].split(" ")[6])
    try:
        mopval.append(int(lines[m * 7 + 2].split(" ")[7]))
    except:
        mopval.append(0)
    test.append(int(lines[m * 7 + 3].strip().split(" ")[3]))
    prod *= test[-1]
    trumonkey.append(int(lines[m * 7 + 4].strip().split(" ")[5]))
    falmonkey.append(int(lines[m * 7 + 5].strip().split(" ")[5]))
    inspectcount.append(0)


for r in range(10000):
    for m in range(monkeys):
        for item in mitems[m]:
            inspectcount[m] += 1
            if mop[m] == '*': 
                if mopval[m] == 0:
                    item *= item % prod # the trick for 11b is to use modulo with the product of all monkey-test-numbers (i cheated and looked it up)
                else:
                    item *= mopval[m] % prod
            else:
                if mopval[m] == 0:
                    item += item
                else:
                    item += mopval[m]
            #item = int(item / 3)
            #print("Item:", item, "div: ", item % test[m])
            if item % test[m] == 0:
                #item = item // test[m]
                mitems[trumonkey[m]].append(item)
            else:
                mitems[falmonkey[m]].append(item)
        mitems[m] = []

s = inspectcount.sort()
print(inspectcount[-1] * inspectcount[-2])    

