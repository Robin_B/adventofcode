line = open("input.txt", "r").readline()
mlen = 14
for i in range(mlen, len(line)):
    doublefound = False
    for r in range(1,mlen):
        if line[i-r] in line[i-mlen:i-r]:
            doublefound = True
            break
    if not doublefound:
        print(line[i-mlen:i], i)
        break
