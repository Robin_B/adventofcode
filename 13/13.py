lines = open("input.txt", "r").readlines()
score = 0
for comp in range((len(lines) + 1) // 3):
    l1 = lines[comp * 3].strip()
    l2 = lines[comp * 3 + 1].strip()
    i1 = 0
    i2 = 0
    print(l1, l2)

    isSmaller = True

    while isSmaller:
        print("Testing: ", l1[i1:], l2[i2:])
        if l1[i1] == ',': i1 += 1
        if l2[i2] == ',': i2 += 1

        if l1[i1] == '[' and l2[i2] == '[':
            i1 += 1
            i2 += 1
            continue
        if l2[i2] == ']' and l1[i1] != ']':
            isSmaller = False
            print ("test failed, left is longer");
            break
        if l1[i1] == ']' and l2[i2] == ']':
            i1 += 1
            i2 += 1
            if i1 >= len(l1):
                break
            continue
        if l1[i1] == ']' and l2[i2] != ']':
            print("Early break!")
            break
            openB = 1
            i2 += 1
            while openB != 0:
                if l2[i2] == ']':
                    openB -= 1
                if l2[i2] == '[':
                    openB += 1
                i2 += 1

            i1 += 1
            i2 += 1

            if i1 >= len(l1):
                break

            if i2 >= len(l2):
                print("l1 longer? test failed")
                isSmaller = False
                break

            continue
        if l1[i1].isnumeric():
            if l2[i2].isnumeric():
                if int(l1[i1]) > int(l2[i2]):
                    isSmaller = False
                    print ("test failed, left int bigger");
                    break
                elif int(l1[i1]) < int(l2[i2]):
                    print ("test succeded, left int smaller");
                    break
                i1 += 1
                i2 += 1
                continue
            else:
                l1 = l1[:i1-1] + ' [' + l1[i1] + ']' + l1[i1+1:]
                continue
        else:
            if l2[i2].isnumeric():
                l2 = l2[:i2-1] + ' [' + l2[i2] + ']' + l2[i2+1:]
                continue     
            else:
                print("weird edge case: ", l1[i1], l2[i2])           
    if isSmaller: score += comp + 1
    print("test complete: ", l1, l2, isSmaller, score)