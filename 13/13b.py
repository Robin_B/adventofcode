from functools import cmp_to_key

lines = open("input.txt", "r").readlines()
score = 0

def checkSubLists(l1, l2):
    print("checking", l1, l2)
    if type(l1) == int:
        if not type(l2) == int:
            return checkSubLists([l1], l2)
        else:
            if l1 < l2: return 1
            if l1 > l2: return -1
            return 0
    if type(l2) == int:
        if not type(l1) == int:
            return checkSubLists(l1, [l2])

    for i in range(len(l1)):
        if i >= len(l2):
            print("l1 longer than l2")
            return -1
        res = checkSubLists(l1[i], l2[i]) 
        if res != 0: return res
    if len(l1) < len(l2): return 1
    print("still 0")
    return 0

lists = [[[2]], [[6]]]
for comp in range((len(lines) + 1) // 3):
    lists.append(eval(lines[comp * 3].strip()))
    lists.append(eval(lines[comp * 3 + 1].strip()))

s = sorted(lists, key=cmp_to_key(checkSubLists), reverse = True)

print("result:", s)
score = (1 + s.index([[2]])) * (1 + s.index([[6]]))
print("score", score)
