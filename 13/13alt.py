lines = open("input.txt", "r").readlines()
score = 0

def checkSubLists(l1, l2):
    print("checking", l1, l2)
    if type(l1) == int:
        if not type(l2) == int:
            return checkSubLists([l1], l2)
        else:
            if l1 < l2: return 1
            if l1 > l2: return -1
            return 0
    if type(l2) == int:
        if not type(l1) == int:
            return checkSubLists(l1, [l2])

    for i in range(len(l1)):
        if i >= len(l2):
            print("l1 longer than l2")
            return -1
        res = checkSubLists(l1[i], l2[i]) 
        if res != 0: return res
    if len(l1) < len(l2): return 1
    print("still 0")
    return 0


for comp in range((len(lines) + 1) // 3):
    l1 = eval(lines[comp * 3].strip())
    l2 = eval(lines[comp * 3 + 1].strip())
    i1 = 0
    i2 = 0
    r = checkSubLists(l1, l2)
    if r == 1: 
        score += comp + 1
    print("result:", r, score)

