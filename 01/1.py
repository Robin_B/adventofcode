f = open("input.txt", "r")

maxCal = []
calCount = 0
for line in f:
    line = line.strip()
    if len(line) == 0:
        maxCal.append(calCount)
        calCount = 0
    else:
        calCount += int(line)
maxCal.sort()
print(sum(maxCal[-3:]))
