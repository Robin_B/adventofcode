from itertools import permutations 

lines = open("input.txt", "r").readlines()

shapes = [((0,0),(1,0),(2,0),(3,0)), ((1,0),(0,1),(1,1),(2,1),(1,2)),((2,0),(2,1),(0,2),(1,2),(2,2)),((0,0),(0,1),(0,2),(0,3)),((0,0), (0,1), (1,0), (1,1))]
heightOffset = [(2,-4), (2, -6), (2, -6), (2, -7), (2, -5)]
widths = [4, 3, 3, 1, 2]

dirs = lines[0].strip()

room = []
falling = False
currentRock = -1
rockCount = 0
rx = 0
ry = 0
windIndex = 0
firstOccupiedSpace = [-1, -1, -1, -1, -1, -1 , -1]
offset = 0
offsetSkipped = 0
firstLoopHeight = 0
secondLoopHeight = 0
previousOffset = 0
partsInLastLoop = 0
justStarted = False

def occupied(x, y):
    global room, offset
    if x < 0 or x >= 7: return True
    if y < 0: return True
    if y >= len(room) + offset: return False
    if len(room) == 0: return False
    return room[y - offset][x] == 1 

while rockCount < 1000000000001:
    justStarted = False
    if not falling:
        if min(firstOccupiedSpace) > offset:
            #print("deleting extra rows. firstOccupiedSpace:", firstOccupiedSpace, "offset:", offset)
            #for r in range(len(room)):
            #    print(room[len(room) - 1 - r])
            toDel = min(firstOccupiedSpace) - offset 
            offset = min(firstOccupiedSpace)
            room = room[toDel:]
            #print("new room: offset:", offset, "len room:", len(room))
            #for r in range(len(room)):
            #    print(room[len(room) - 1 - r])
        falling = True
        currentRock = (currentRock + 1) % len(shapes)
        if currentRock == 0: justStarted = True
        rx = heightOffset[currentRock][0]
        ry = len(room) + offset - heightOffset[currentRock][1] - 1
        rockCount += 1
        if rockCount % 1000 == 0: print(rockCount)
        #print("New rock type", currentRock, "at", rx, ry)

    # idea for round b: assume that whenever we went through the entire wind direction list, the top of the room
    # will look similar. then we can just measure how many rocks were in this 'wind block', and simulate forward by
    # counting how many 'wind blocks fit into the remaining rocks required'
    if windIndex == 0:
        if firstLoopHeight == 0:
            firstLoopHeight = offset
            partsInLastLoop = rockCount
            print("firstLoopHeight:", firstLoopHeight)
        else:
            if secondLoopHeight == 0:
                secondLoopHeight = offset - firstLoopHeight
                rocksToSkip = rockCount - partsInLastLoop
                skipCount = (1000000000001 - rockCount) // rocksToSkip
                rockCount += rocksToSkip * skipCount
                offsetSkipped = secondLoopHeight * skipCount
                print("secondLoopHeight:", secondLoopHeight, "skipped to rocks: ", rockCount, "rocks skipped per jump:", rocksToSkip, "offset skipped:", secondLoopHeight)
            else:
                if offset - previousOffset == secondLoopHeight:
                    print("MATCH!")
                else:
                    print("NO MATCH: ", offset - previousOffset)
        previousOffset = offset

        print("windex 0, height:", len(room), "offset:", offset)

        for r in range(len(room)):
            print(room[len(room) - 1 - r])

    # move with wind
    if dirs[windIndex] == '<':
        if rx > 0:
            for parts in shapes[currentRock]:
                movable = True
                if occupied(rx + parts[0] - 1, ry - parts[1]):
                    movable = False
                    break
            if movable: rx -= 1
    else:
        if rx < 7 - widths[currentRock]:
            for parts in shapes[currentRock]:
                movable = True
                if occupied(rx + parts[0] + 1, ry - parts[1]):
                    movable = False
                    break
            if movable: rx += 1
    #print("Wind: ",dirs[windIndex])
    windIndex = (windIndex + 1) % len(dirs)

    
    # move down
    grounded = False
    #if ry == -heightOffset[currentRock][1] - 3:
    #    grounded = True
    #else:
    for parts in shapes[currentRock]:
        if occupied(rx + parts[0], ry - parts[1] - 1):
            grounded = True
            break

    if not grounded:
        ry -= 1
    else:
        for parts in shapes[currentRock]:
            yheight = ry - parts[1]
            while len(room) + offset <= yheight:
                room.append([0,0,0,0,0,0,0])
            #print("yheight:", yheight, "offset:", offset, "room height:", len(room))
            room[yheight - offset][rx + parts[0]] = 1
            if firstOccupiedSpace[rx + parts[0]] < yheight:
                firstOccupiedSpace[rx + parts[0]] = yheight
        falling = False



    #print("Current Rock", rockCount, "pos:", rx, ry, "Room:")
    #for r in range(len(room)):
    #    print(room[len(room) - 1 - r])

print("Room height: ", len(room) + offset + offsetSkipped)

#1514285714288
#1565242165201