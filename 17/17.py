from itertools import permutations 

lines = open("input.txt", "r").readlines()

shapes = [((0,0),(1,0),(2,0),(3,0)), ((1,0),(0,1),(1,1),(2,1),(1,2)),((2,0),(2,1),(0,2),(1,2),(2,2)),((0,0),(0,1),(0,2),(0,3)),((0,0), (0,1), (1,0), (1,1))]
heightOffset = [(2,-4), (2, -6), (2, -6), (2, -7), (2, -5)]
widths = [4, 3, 3, 1, 2]

dirs = lines[0].strip()

room = []
falling = False
currentRock = -1
rockCount = 0
rx = 0
ry = 0
windIndex = 0

def occupied(x, y):
    global room
    if x < 0 or x >= 7: return True
    if y < 0: return True
    if y >= len(room): return False
    if len(room) == 0: return False
    return room[y][x] == 1 

while rockCount < 2023:
    if not falling:
        falling = True
        currentRock = (currentRock + 1) % len(shapes)
        rx = heightOffset[currentRock][0]
        ry = len(room) - heightOffset[currentRock][1] - 1
        rockCount += 1
        print("New rock type", currentRock, "at", rx, ry)


    # move with wind
    if dirs[windIndex] == '<':
        if rx > 0:
            for parts in shapes[currentRock]:
                movable = True
                if occupied(rx + parts[0] - 1, ry - parts[1]):
                    movable = False
                    break
            if movable: rx -= 1
    else:
        if rx < 7 - widths[currentRock]:
            for parts in shapes[currentRock]:
                movable = True
                if occupied(rx + parts[0] + 1, ry - parts[1]):
                    movable = False
                    break
            if movable: rx += 1
    #print("Wind: ",dirs[windIndex])
    windIndex = (windIndex + 1) % len(dirs)
    # move down
    grounded = False
    #if ry == -heightOffset[currentRock][1] - 3:
    #    grounded = True
    #else:
    for parts in shapes[currentRock]:
        if occupied(rx + parts[0], ry - parts[1] - 1):
            grounded = True
            break

    if not grounded:
        ry -= 1
    else:
        for parts in shapes[currentRock]:
            yheight = ry - parts[1]
            while len(room) <= yheight:
                room.append([0,0,0,0,0,0,0])
            room[yheight][rx + parts[0]] = 1
        falling = False



    #print("Current Rock", rockCount, "pos:", rx, ry, "Room:")
    #for r in range(len(room)):
    #    print(room[len(room) - 1 - r])

print("Room height: ", len(room))