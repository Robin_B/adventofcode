f = open("input.txt", "r")

score = 0
for line in f:
    sections = line.strip().split(",")
    s1 = sections[0].split("-")
    s2 = sections[1].split("-")
    # check if one section's start is within the range of the other
    if (int(s1[0]) <= int(s2[0]) and int(s1[1]) >= int(s2[0])) or (int(s2[0]) <= int(s1[0]) and int(s2[1]) >= int(s1[0])):
        score += 1
print(score)