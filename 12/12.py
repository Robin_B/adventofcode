lines = open("input.txt", "r").readlines()

sx = 0
sy = 0
tx = 0
ty = 0

m = []
visited = []
pool = []
currentPathLen = 0
def getHeuristic(x, y):
    global currentPathLen, tx, ty
    return currentPathLen + abs(tx - x) + abs(ty - y)

def getCandidates():
    global m, sx, sy, visited, currentPathLen
    candidates = []
    currentPathLen += 1
    if sx > 0 and ord(m[sx][sy]) >= ord(m[sx - 1][sy]) - 1 and visited[sx - 1][sy] > currentPathLen:
        candidates.append((sx - 1, sy, getHeuristic(sx - 1, sy), currentPathLen))
    if sx < len(m)-1 and ord(m[sx][sy]) >= ord(m[sx + 1][sy]) - 1 and visited[sx + 1][sy] > currentPathLen:
        candidates.append((sx + 1, sy, getHeuristic(sx + 1, sy), currentPathLen))
    if sy > 0 and ord(m[sx][sy]) >= ord(m[sx][sy - 1]) - 1 and visited[sx][sy - 1] > currentPathLen:
        candidates.append((sx, sy - 1, getHeuristic(sx, sy - 1), currentPathLen))
    if sy < len(m[0])-1 and ord(m[sx][sy]) >= ord(m[sx][sy + 1]) - 1 and visited[sx][sy + 1] > currentPathLen:
        candidates.append((sx, sy + 1, getHeuristic(sx, sy + 1), currentPathLen))
    return candidates

for l in lines:
    if l.find('S') > -1:
        sy = l.find('S')
        sx = len(m)
        l = l.replace('S', 'a')
    if l.find('E') > -1:
        ty = l.find('E')
        tx = len(m)
        l = l.replace('E', 'z')
    m.append(l.strip())
    visited.append([1000 for _ in l])

for x in range(len(m)):
    for y in range(len(m[0])):
        if m[x][y] == 'a':
            #visited[x][y] = 0
            pool.append((x, y, getHeuristic(x, y), 0))
pool.sort(key= lambda x: x[2], reverse=True)
print("start pool:", pool)
searches = 0
print("s:", sx, sy, "t:", tx, ty)

while not (sx == tx and sy == ty):
    visited[sx][sy] = currentPathLen
    nc = getCandidates()
    searches += 1
    #print("new candidates:", nc)
    #pool.extend(nc)
    for n in nc:
        pos = 0
        for p in pool:
            if n[3] > p[3]:
                break
            pos += 1
        pool.insert(pos, n)
            
    #pool.sort(key= lambda x: x[2], reverse=True)
    #print("all candidates:", pool)
    best = pool.pop()
    #print("best:", best)
    #for v in visited:
    #    for vv in v:
    #        if vv == 1000:
    #            print('-', end='')
    #        else:
    #            print(vv, end='')
    #    print()

    currentPathLen = best[3]
    sx = best[0]
    sy = best[1]    
    while visited[sx][sy] == currentPathLen:
        best = pool.pop()
        currentPathLen = best[3]
        sx = best[0]
        sy = best[1]  
    #print("Going to", sx, sy, currentPathLen, ord(m[sx][sy]))

if(sx == tx and sy == ty):
    print("Found Target!, currentPathLen", currentPathLen, "searches:", searches)

