f = open("input.txt", "r")
outcomeA = [1, 0, 2]
outcomes = [[2, 0, 1], [0, 1, 2], [1, 2, 0]]
score = 0
for line in f:
    line = line.strip()
    other = ord(line[0]) - ord('A')
    result = ord(line[2]) - ord('X')
    
    score += result * 3 + outcomes[other][result] + 1

    print(line, other, result, score)


