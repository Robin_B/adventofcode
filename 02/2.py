f = open("input.txt", "r")
outcomeA = [3, 6, 0]
score = 0
for line in f:
    line = line.strip()
    other = ord(line[0]) - ord('A')
    mine = ord(line[2]) - ord('X')
    score += mine + 1 + outcomeA[(3 + mine - other) % 3]

    print(line, other, mine, score)


