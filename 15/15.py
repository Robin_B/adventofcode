lines = open("input.txt", "r").readlines()

sensors = []

for line in lines:
    words = line.strip().split()
    sensors.append((int(words[2][2:-1]), int(words[3][2:-1]), int(words[8][2:-1]), int(words[9][2:])))

checkLine = 2000000
scan = {}
beacons = {}
for s in sensors:
    bdist = abs(s[0] - s[2]) + abs(s[1] - s[3])
    #print(s, bdist)
    ydist = abs(s[1] - checkLine)
    if ydist <= bdist:
        spread = bdist - ydist
        for x in range(s[0] - spread, s[0] + spread + 1):
            scan[x] = 1
            if x == s[1] and s[3] == checkLine:
                beacons[x] = 1
        #print(scan)


print(len(scan), len(beacons))
print("score: ", len(scan) - len(beacons))