lines = open("input.txt", "r").readlines()

sensors = []

for line in lines:
    words = line.strip().split()
    sensors.append((int(words[2][2:-1]), int(words[3][2:-1]), int(words[8][2:-1]), int(words[9][2:])))

xlim = 4000000
print(2727057 * xlim + 2916597)
borders = {}

for s in sensors:
    bdist = abs(s[0] - s[2]) + abs(s[1] - s[3])
    print(s, bdist)
    
    for b in list(borders):
        if abs(s[0] - b[0]) + abs(s[1] - b[1]) <= bdist:
            borders.pop(b)

    for i in range(bdist + 1):
        x = s[0] + i
        y = s[1] - (bdist + 1) + i
        if x >= 0 and x <= xlim and y >= 0 and y <= xlim:
            borders[(x, y)] = 1

        x = s[0] + i
        y = s[1] + (bdist + 1) - i
        if x >= 0 and x <= xlim and y >= 0 and y <= xlim:
            borders[(x, y)] = 1

        x = s[0] - i
        y = s[1] - (bdist + 1) + i
        if x >= 0 and x <= xlim and y >= 0 and y <= xlim:
            borders[(x, y)] = 1

        x = s[0] - i
        y = s[1] + (bdist + 1) - i
        if x >= 0 and x <= xlim and y >= 0 and y <= xlim:
            borders[(x, y)] = 1



for s in sensors:
    bdist = abs(s[0] - s[2]) + abs(s[1] - s[3])
    #print(s, bdist)
    
    for b in list(borders):
        if abs(s[0] - b[0]) + abs(s[1] - b[1]) <= bdist:
            borders.pop(b)

print(borders)