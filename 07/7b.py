lines = open("input.txt", "r").readlines()
index = 0
dirs = []
alldirs = []

def scanDir():
    global lines, index, alldirs
    size = 0
    l = lines[index]
    print("line: ", l)
    index += 1
    while index <= len(lines):
        s = l.strip().split(" ")
        if s[0] == '$':
            if s[1] == 'cd':
                if s[2] == "..":
                    print("dirsize: ", size)
                    alldirs.append(size)
                    return size
                else:
                    print("entering dir ", s[2])
                    size += scanDir()
        else:
            if s[0].isdecimal():
                size += int(s[0])
                print('adding', s[0], size)
        if index == len(lines): 
            print ("file end, size:", size)
            alldirs.append(size)
            return size
        l = lines[index]
        index += 1
    print ("loop end, size:", size)
    alldirs.append(size)
    return size

scanDir()
freesize = 70000000 - alldirs[-1]
missing = 30000000 - freesize
alldirs.sort()
for d in alldirs[:-1]:
    if d >= missing:
        print(d)
        break

