lines = open("input.txt", "r").readlines()
index = 0
dirs = []
alldirs = []

def scanDir():
    global lines, index, alldirs
    size = 0
    l = lines[index]
    index += 1
    while index <= len(lines):
        s = l.strip().split(" ")
        if s[0] == '$' and s[1] == 'cd':
                if s[2] == "..":
                    alldirs.append(size)
                    return size
                else:
                    size += scanDir()
        else:
            if s[0].isdecimal():
                size += int(s[0])
        if index == len(lines): 
            alldirs.append(size)
            return size
        l = lines[index]
        index += 1

scanDir()
missing = 30000000 - (70000000 - alldirs[-1])
alldirs.sort()
for d in alldirs:
    if d >= missing:
        print(d)
        break

