lines = open("input.txt", "r").readlines()
index = 0
dirs = []
smalldirs = 0

def scanDir():
    global lines, index, smalldirs
    size = 0
    l = lines[index]
    print("line: ", l)
    index += 1
    while index < len(lines):
        s = l.strip().split(" ")
        if s[0] == '$':
            if s[1] == 'cd':
                if s[2] == "..":
                    print("dirsize: ", size)
                    if size < 100000: 
                        smalldirs += size
                    return size
                else:
                    print("entering dir ", s[2])
                    size += scanDir()
        else:
            if s[0].isdecimal():
                size += int(s[0])
                print('adding', s[0], size)
        if index == len(lines): return size
        l = lines[index]
        index += 1
    return size

print(scanDir())

print("small dirs: ", smalldirs)

